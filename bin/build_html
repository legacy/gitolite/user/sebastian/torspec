#!/usr/bin/env bash

set -e -u -o pipefail -x

: "${MDBOOK:=mdbook}"

TOPLEVEL=$(realpath "$(dirname "$0")")/..
cd "${TOPLEVEL}"
./bin/reindex

./bin/make_redirects

# Now we deal with mermaid diagrams.
#
# This may require changes to the md files in {spec,proposals}
# or to the mdbook.toml files in mdbook/*/.
# We will make a copy of whatever we need to change,
# and then make changes to that copy.
#
# When we are done with these changes, we will set some variables:
# - MDBOOK_OUTPUT_DIR is "", or the location where we have put our raw mdbook output.
# - MDBOOK_DIR is the parent directory of the possibly modified copies
#     of mdbook/{spec,proposals}.

if test -n "${MMDC:-}" || command -v "mmdc" >&/dev/null; then
    # CASE 1: mermaid-cli is installed.
    #
    # We will convert mermaid diagrams to svg.  The mermaid_cvt_svg
    # script does this with a temporary copy of our markdown directories,
    # so as not to alter the original.
    #
    # (The conversion involves npm and a headless chrome browser,
    # to it is understandable that not everybody would want to do it
    # this way.)
    echo "Using mermaid-cli to pre-render mermaid diagrams"

    TMPDIR=$(mktemp -d "${TOPLEVEL}/tmp_mmdc.XXXXXXXX")
    trap 'rm -rf "$TMPDIR"' 0
    ./bin/mermaid_cvt_svg "$TMPDIR"
    MDBOOK_OUTPUT_DIR="$TMPDIR/build"
    MDBOOK_DIR="$TMPDIR/mdbook"
elif test -n "${MDBOOK_MERMAID:-}" || command -v "mdbook-mermaid" >&/dev/null; then
    # CASE 2: mdbook_mermaid is installed.
    #
    # We will make a temporary copy of the mdbook configuration directory
    # only, and use mdbook-mermaid to alter that.
    #
    # This is much easier to run locally, but it requires that your
    # browser has enough client-side javascript in order to run
    # mermaid.  It doesn't touch npm.
    echo "Using mdbook-mermaid to set up dynamic rendering of mermaid diagrams"

    MDBOOK_MERMAID=${MDBOOK_MERMAID:=mdbook-mermaid}
    TMPDIR=$(mktemp -d "${TOPLEVEL}/tmp_mdbook_mermaid.XXXXXXXX")
    trap 'rm -rf "$TMPDIR"' 0
    cp -r ./mdbook/proposals ./mdbook/spec ./mdbook/theme "$TMPDIR"
    mdbook-mermaid install "$TMPDIR/spec"
    mdbook-mermaid install "$TMPDIR/proposals"
    MDBOOK_OUTPUT_DIR=""
    MDBOOK_DIR="$TMPDIR"
else
    # CASE 3: No mermaid support.
    #
    # In this case we run mdbook on our inputs unchanged.
    # The mermaid blocks will render as code.
    echo "No mermaid support found; mermaid diagrams will be unrendered"

    MDBOOK_OUTPUT_DIR=""
    MDBOOK_DIR="$TOPLEVEL/mdbook"
fi

# mdbook-linkcheck is a non-obvious dependency, and the mdbook output when it's
# not found doesn't spell out how to install it.
if ! command -v mdbook-linkcheck; then
    echo 'ERROR: mdbook-linkcheck not found. You should probably install it with "cargo install mdbook-linkcheck"'
    exit 1
fi

$MDBOOK build "${MDBOOK_DIR}/spec"
$MDBOOK build "${MDBOOK_DIR}/proposals"

if test -n "${MDBOOK_OUTPUT_DIR}"; then
   rm -rf "${TOPLEVEL}/build"
   mv "${MDBOOK_OUTPUT_DIR}" "${TOPLEVEL}/build"
fi

rm -rf "${TOPLEVEL}/html/"
mv "${TOPLEVEL}/build/spec/html" "${TOPLEVEL}/html"
mv "${TOPLEVEL}/build/proposals/html" "${TOPLEVEL}/html/proposals"

