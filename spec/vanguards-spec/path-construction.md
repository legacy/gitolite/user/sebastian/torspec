# Path Construction

Both vanguards systems use a mesh topology: this means that circuits select
a hop from each layer independently, allowing paths from any relay in a
layer to any relay in the next layer.

## Selecting Relays

Vanguards relays are selected from relays with the Stable and Fast flags.

Tor replaces a vanguard whenever it is no longer listed in the most
recent consensus, with the goal that we will always have the right
number of vanguards ready to be used.

For implementation reasons, we also replace a vanguard if it loses
the Fast or Stable flag, because the path selection logic wants middle
nodes to have those flags when it's building preemptive vanguard-using
circuits.

The design doesn't have to be this way: we might instead have chosen to keep
vanguards in our list as long as possible, and continue to use them even if
they have lost some flags. This tradeoff is similar to the one in [Bug
#17773](https://bugs.torproject.org/17773), about whether to continue using
Entry Guards if they lose the Guard flag -- and Tor's current choice is "no,
rotate" for that case too.

## Path Restriction Changes

Path restrictions, as well as the ordering of their application, are currently
extremely problematic, resulting in information leaks with this topology.
Until they are reworked, we disable many of them for onion service circuits.

In particular, we allow the following:
   1. Nodes from the same /16 and same family for any/all hops in a circuit
   2. Guard nodes can be chosen for RP/IP/HSDIR
   3. Guard nodes can be chosen for hop before RP/IP/HSDIR.

The first change prevents the situation where paths cannot be built if two
layers all share the same subnet and/or node family, or if a layer consists of
only one family and that family is the same as the RP/IP/HSDIR. It also
prevents the the use of a different entry guard based on the family or subnet
of the IP, HSDIR, or RP. (Alternatives of this permissive behavior are
possible: For example, each layer could ensure that it does not consist solely
of the same family or /16, but this property cannot easily apply to conflux
circuits).

The second change prevents an adversary from forcing the use of a different
entry guard by enumerating all guard-flagged nodes as the RP. This change is
important once onion services support conflux.

The third change prevents an adversary from learning the guard node by way
of noticing which nodes were not chosen for the hop before it. This change is
important once onion services support conflux.
