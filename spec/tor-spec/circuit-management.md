<a id="tor-spec.txt-5"></a>

# Circuit management{#circuit-management}

This section describes how circuits are created, and how they operate
once they are constructed.
